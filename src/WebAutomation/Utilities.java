package WebAutomation;
import WebAutomation.RegistrationForm;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class Utilities {
	RegistrationForm rf;
	WebDriver driver = RegistrationForm.driver;
	WebElement fname = driver.findElement(By.id("fullname"));
	WebElement fnameText = driver.findElement(By.className("helpText"));
	WebElement fnameLabel = driver.findElement(By.className("formTitle"));
	WebElement email = driver.findElement(By.id("email"));
	WebElement emailHelpText = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[2]/div/div[2]"));
	WebElement mNumber = driver.findElement(By.id("mobnumber"));
	WebElement mobHelpText = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[3]/div/div[2]"));
	WebElement city = driver.findElement(By.id("city"));
	WebElement cityOption = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[4]/div/select/option[3]"));
    WebElement operation = driver.findElement(By.id("operation"));
    WebElement operationOption = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[5]/div/select/option[3]"));
    WebElement businessname = driver.findElement(By.id("businessname"));
    WebElement companyTurnover = driver.findElement(By.name("company_turnover"));
   // driver.findElement(By.name("company_turnover"))

    WebElement turnoverOption = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[7]/div/select/option[4]"));
    WebElement sellOnline = driver.findElement(By.id("selling_online"));
    WebElement sellOption = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[9]/div/select/option[2]"));
    WebElement marketChkBox = driver.findElement(By.className("checkboxGroup"));
    WebElement myntra = driver.findElement(By.xpath("//input[@id='myntra']"));
    WebElement snapdeal = driver.findElement(By.xpath("//input[@id='snapdeal']"));
    WebElement amazon = driver.findElement(By.xpath("//input[@id='amazon']"));
    WebElement flipkart = driver.findElement(By.xpath("//input[@id='flipkart']"));
    WebElement ebay = driver.findElement(By.xpath("//input[@id='ebay']"));
    WebElement shopclues = driver.findElement(By.xpath("//input[@id='shopclues']"));
    WebElement paytm = driver.findElement(By.xpath("//input[@id='paytm']"));
    WebElement others = driver.findElement(By.xpath("//input[@id='others']"));
    WebElement terms = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[11]/div/input"));
    WebElement signup = driver.findElement(By.id("signup_btn"));
    //WebElement signupHelpText = driver.findElement(By.id("submit_alert"));

    WebElement signupHelpText = driver.findElement(By.xpath("//*[@id=\"submit_alert\"]"));
    WebElement helpCitytext = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[4]/div/div[2]"));
    WebElement helpOperatetext = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[5]/div/div[2]"));
    WebElement helpTurnOvertext = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[7]/div/div[2]"));
    WebElement helpsellOnlinetext = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[9]/div/div[2]"));
    WebElement helpCompanyNametext = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[6]/div/div[2]")); 
    
    public void scrollBrowser(String id){
    	JavascriptExecutor js = (JavascriptExecutor) driver;		
		js.executeScript("document.getElementById('id').scrollIntoView(true);");
    }
    public void fillFullName(String key){
    	fname.sendKeys(key);
	}
    
    public void fillEmail(String key){
    	email.sendKeys(key);
    }
    
    public void fillMobNumber(String key){
    	mNumber.sendKeys(key);
    }
    
    public void chooseCity(int index){
    	city.click();
    	Select drop = new Select(city);
    	drop.selectByIndex(index);
    }
   
    /*
     * 2 - Private Limited
     * 3 - Partnership
     * 4 - Sole Proprietorship
     * 5 - Limited Company (Unlisted)
     * 6 - Other
     */
   
    public void chooseOperation(int index){
    	Select drop = new Select(operation);
    	drop.selectByIndex(index);
    }
    
    public void fillBusinessName(String key){
    	businessname.sendKeys(key);
    }
    
    /*
    <25        : 1
    25L - 50L  : 2
    50L - 1Cr  : 3
    1Cr - 2Cr  : 4
    2Cr - 3Cr  : 5
    3Cr - 4Cr  : 6
    4Cr - 5Cr  : 7
    5Cr - 6Cr  : 8
    6Cr - 7Cr  : 9
    7Cr - 8Cr  : 10
    8Cr - 9Cr  : 11
    9Cr - 10Cr : 12
    >10Cr      : 13
    */
    
    public void chooseCompanyTurnover(int index){
    	Select drop = new Select(companyTurnover);
    	drop.selectByIndex(index);
    }
    
    public void chooseRequiredLoan(int num){
    	WebElement we = driver.findElement(By.xpath("/html/body/div[2]/div/div[1]/form/div[8]/div/div[3]/div/span/span[1]"));
		Actions move = new Actions(driver);
		
		move.dragAndDropBy(we, 50, 0).build().perform();
    }
    
    /*
     *    Yes - 2
     *    No - 3
     */   
    public void chooseSellOnline(int index){
    	Select drop = new Select(sellOnline);
    	drop.selectByIndex(index);
    }
    
    //marketChkBox
    public void chooseMarketChkBox(String key ){
    	//scrollBrowser(driver, "myntra");
    	JavascriptExecutor js = (JavascriptExecutor) driver;
    	driver.manage().timeouts().implicitlyWait(400, TimeUnit.SECONDS);
		js.executeScript("document.getElementById('myntra').scrollIntoView(true);");
		driver.manage().timeouts().implicitlyWait(800, TimeUnit.SECONDS);
    		switch(key){
    		case "myntra":
    			driver.findElement(By.xpath("//input[@id='myntra']")).click();
    			break;
    		
    		case "snapdeal":
    			driver.findElement(By.xpath("//input[@id='snapdeal']")).click();
    			break;
    			
    		case "amazon":
    			driver.findElement(By.xpath("//input[@id='amazon']")).click();
    			break;
    			
    		case "flipkart":
    			driver.findElement(By.xpath("//input[@id='flipkart']")).click();
    			break;
    			
    		case "ebay":
    			driver.findElement(By.xpath("//input[@id='ebay']")).click();
    			break;
    			
    		case "shopclues":
    			driver.findElement(By.xpath("//input[@id='shopclues']")).click();
    			break;
    			
    		case "paytm":
    			driver.findElement(By.xpath("//input[@id='paytm']")).click();
    			break;
    			
    		case "others":
    			driver.findElement(By.xpath("//input[@id='others']")).click();
    			break;
    			
    		}	
    }
    
    public void setValues(String name, String email, 
    		String mobNum, String city, String operate, String company, 
    		int turnOver, int num, int sellonline){
    	fillFullName(name);
		fillEmail(email);
		fillMobNumber(mobNum);
		chooseCity(2);
		chooseOperation(2);
		//ut.fillBusinessName("Company");
		chooseCompanyTurnover(turnOver);		
		chooseRequiredLoan(num);
		chooseSellOnline(sellonline);
		
		//driver.manage().timeouts().implicitlyWait(600, TimeUnit.SECONDS);
		
		//chooseMarketChkBox(driver, "myntra");
    }
    
    
    
   public void clearValues(){
	   fname.clear();
	   email.clear();
	   mNumber.clear();
	   businessname.clear();
   }

	public String helpName(){
		return fnameText.getText();
	}
	
	public String helpEmail(){
		return emailHelpText.getText();	
	}
	
	public String helpMobNumber(){
		return mobHelpText.getText();
	}
	
	public String helpSignup(){
		return signupHelpText.getText();
	}
	
	public String helpCity(){
		return helpCitytext.getText();
	}
	
	public String helpOperateAs(){
		return helpOperatetext.getText();
	}
	
	public String helpTurnOver(){
		return helpTurnOvertext.getText();
	}
	
	public String helpSellOnline(){
		return helpsellOnlinetext.getText();
	}
	
	public String helpCompanyName(){
		return helpCompanyNametext.getText();
	}
}
