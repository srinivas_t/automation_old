package WebAutomation; 

import WebAutomation.Utilities;
import java.util.concurrent.TimeUnit;
import java.lang.Object;

import java.util.*;
import org.openqa.selenium.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

import org.testng.Assert;
import org.testng.annotations.*;
import org.openqa.selenium.support.ui.Select;


public class RegistrationForm {

	Utilities ut;
	public static WebDriver driver;// = new FirefoxDriver();
	String url;
	String username = "fullname";
	String email = "email";
	String mobile_number = "mobnumber";
	//String loginLink = 
	public RegistrationForm(String browser){
		System.setProperty("webdriver.chrome.driver","C://SRI/Capitalfloat/Automation_path/ChromeDriver/chromedriver.exe");
		switch(browser){
		case "Firefox":
			driver = new FirefoxDriver();
			break;
		case "Chrome":
			driver = new ChromeDriver();
			break;

		}
		url = "http://capitalfloat.com";
	}

	public void openSite(){
		driver.get(url);
		driver.manage().window().maximize();
	}

	public boolean checkSite(String title){
		//String expectedTitle = "Capital Float - Smart Business loans for SMEs in India";
		String actualTitle = driver.getTitle();
		return title == actualTitle;
	}

	public void openLoginPage(){
		driver.findElement(By.linkText("LOG IN")).click();
		if(!checkSite("Login")){
			//log error that unable to open page
		}
	}

	public void openRegistrationPage(){
		driver.findElement(By.linkText("Register Now")).click();
		if(!checkSite("Register")){
			//log error that unable to open page
		}
	}

	@BeforeTest
	public void setUp(){
		System.setProperty("webdriver.chrome.driver", "C://Anupama/Softwares/chromedriver_win32/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://capitalfloat.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		driver.findElement(By.linkText("LOG IN")).click();
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		driver.findElement(By.linkText("Register Now")).click();
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		ut = new Utilities();

	}

	@AfterMethod
	public void clear(){
		ut.clearValues();
	}

	//Enter a number for name, it will be accepted
	@Test
	public void nameNum(){
		ut.fillFullName("123456");
		ut.fnameLabel.click();
		Assert.assertEquals(ut.helpName(), "");

	}

	//Enter a string with digits and numbers for name, it will be accepted
	@Test
	public void nameAlphaNum(){
		ut.fillFullName("12abhgt3r456");
		ut.fnameLabel.click();
		Assert.assertEquals(ut.helpName(), "");

	}

	/*Don't enter value for name, this throws a message that 
	 * full name has to be entered
	 */
	@Test
	public void nameNull(){
		ut.fillFullName("test");
		ut.fname.clear();
		ut.fnameLabel.click();
		Assert.assertEquals(ut.helpName(), "please enter your full name");

	}

	/*Don't enter value for email, this throws a message that 
	 * full name has to be entered
	 */
	@Test
	public void emailNull(){
		ut.fillEmail("test@gmail.com");
		ut.email.clear();
		ut.fnameLabel.click();
		Assert.assertEquals(ut.helpEmail(), "please enter your email address");

	}

	/*
	enter an email id which doesn't have .com* at the end. 
	This is valid and no error is thrown.
	 */
	@Test
	public void emailDomain(){
		ut.fillEmail("123456789@gmail");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpEmail(), "");	

	}

	//Enter an invalid email id, which throws an error massage
	@Test
	public void emailInvalid(){
		ut.fillEmail("test");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpEmail(), "please enter your email in proper format");	

	}

	//Enter email with 254 char and see taht its accepted
	@Test
	public void email254char(){
		ut.fillEmail("dfrthhhhyffrmhdfkvkjkhlfkkhjvnhxjfksjchgskslgcnhkgxckhksgk"
				+ "hgkksfhngfjfxxgshkhjxfghksjghjbcxkckgkxnrxgtxgxzmdfrthhhhyffr"
				+ "mhdfkvkjkhlfkhkhjvnhxjfksjchgskslgcnhkgxckhksgkhgkksfhngfjfxxg"
				+ "shkhjxfghksjghjbcxkckgkxnrxgtxgxzmkljckghkfgnxkrkozlhgmkhjgrkh"
				+ "z@hgkjgjjhj");

		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpEmail(), "");	

	}

	//Check that email id with special char result in error
	@Test
	public void emailInvalidSplChar1(){
		ut.fillEmail("#$^&6887@g$ail.com");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpEmail(), "please enter your email in proper format");	

	}

	//Enter a null value, which results in an error
	@Test
	public void mobNull(){
		ut.mNumber.clear();
		ut.fillMobNumber("");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpMobNumber(), "please provide your mobile number");	
	}

	//Alphanumeric value for mobile results in error message
	@Test
	public void mobAlphaNum(){
		ut.fillMobNumber("9abcd$rf78");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpMobNumber(), "please enter a valid number.");


	}

	//
	@Test
	public void mobStart0(){
		ut.fillMobNumber("0123456787");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpMobNumber(), "mobile number has to be a minimum of 10 digits");	
	}

	@Test
	public void mobValid(){
		ut.fillMobNumber("1123456787");
		ut.fnameLabel.click();	
		Assert.assertEquals(ut.helpMobNumber(), "");	
	}

	@Test
	public void cityNull(){
		//ut.clearValues();
		ut.city.click();
		ut.fnameLabel.click();
		Assert.assertEquals(ut.helpCity(), "please enter the city of your residence");
	}


	@Test
	public void operateAsnull(){
		//ut.clearValues();
		ut.operation.click();
		ut.fnameLabel.click();
		Assert.assertEquals(ut.helpOperateAs(), "please choose the appropriate option");
	}

	@Test
	public void companyTurnovernull(){
		//ut.clearValues();
		ut.companyTurnover.click();
		ut.fnameLabel.click();
		Assert.assertEquals(ut.helpTurnOver(), "please specify your company's turnover");
	}

	@Test
	public void sellOnlinenull(){
		//ut.clearValues();
		ut.sellOnline.click();
		ut.fnameLabel.click();
		Assert.assertEquals(ut.helpSellOnline(), "please specify the appropriate option");
	}


	@Test
	public void companyNamenull(){
		//.clearValues();
		ut.businessname.click();
		ut.fnameLabel.click();
		Assert.assertEquals(ut.helpCompanyName(), "please specify your company's name");
	}


	// Test case for null fullName 

	@Test
	public void nullFullName(){

		ut.fillEmail("t.est.c.ap.ita.lfloa.t@gmail.com");
		ut.fillMobNumber("9845220265");
		ut.chooseCity(2);
		ut.chooseOperation(2);
		ut.chooseCompanyTurnover(6);
		ut.fillBusinessName("test");
		//ut.chooseRequiredLoan(20);
		ut.chooseSellOnline(2);
		ut.terms.click();
		ut.signup.click();
		Assert.assertEquals(ut.helpSignup(),"Please complete all the fields before submitting");	


	}

	// Test case for null Email Address	
	@Test
	public void nullEmailAddr(){

		ut.fillFullName("test");
		ut.fillMobNumber("9845220265");
		ut.chooseCity(8);
		ut.chooseOperation(5);
		ut.chooseCompanyTurnover(13);
		ut.fillBusinessName("test");
		//ut.chooseRequiredLoan(20);
		ut.chooseSellOnline(2);
		ut.terms.click();
		ut.signup.click();
		Assert.assertEquals(ut.helpSignup(),"Please complete all the fields before submitting");


	}

	// Test Case for null mobile number	
	@Test
	public void nullMobNumb(){

		ut.fillEmail("t.est.c.ap.ita.lfloa.t@gmail.com");
		ut.fillFullName("test");
		ut.chooseCity(2);
		ut.chooseOperation(2);
		ut.chooseCompanyTurnover(6);
		ut.fillBusinessName("test");
		//ut.chooseRequiredLoan(20);
		ut.chooseSellOnline(2);
		ut.terms.click();
		ut.signup.click();
		Assert.assertEquals(ut.helpSignup(),"Please complete all the fields before submitting");	
	}

	// Test Case for null City	
	@Test
	public void nullCity(){

		ut.fillFullName("test");
		ut.fillEmail("t.est.c.ap.ita.lfloa.t@gmail.com");
		ut.fillMobNumber("9845220265");
		ut.fillBusinessName("test");
		ut.chooseOperation(2);
		ut.chooseCompanyTurnover(6);		
		ut.chooseRequiredLoan(20);
		ut.chooseSellOnline(2);
		ut.terms.click();
		ut.signup.click();
		Assert.assertEquals(ut.helpSignup(),"Please complete all the fields before submitting");	
	}

	// Test Case for null operate as	
	@Test
	public void nullOperateAs(){		

		ut.fillFullName("test");
		ut.fillEmail("t.est.c.ap.ita.lfloa.t@gmail.com");
		ut.fillMobNumber("9845220265");
		ut.chooseCity(2);
		//new Select(driver.findElement(By.id("operation"))).selectByIndex(2);
		ut.chooseCompanyTurnover(6);
		ut.fillBusinessName("test");
		ut.chooseRequiredLoan(20);
		ut.chooseSellOnline(2);
		ut.terms.click();
		ut.signup.click();			
		Assert.assertEquals(ut.helpSignup(),"Please complete all the fields before submitting");	
	}

	// Test Case for null Company Name
	@Test
	public void nullCompanyName(){

		ut.fillFullName("test");
		ut.fillEmail("t.est.c.ap.ita.lfloa.t@gmail.com");
		ut.fillMobNumber("9845220265");
		ut.chooseCity(2);
		ut.chooseOperation(2);
		ut.chooseCompanyTurnover(6);
		//ut.fillBusinessName("test");
		//ut.chooseRequiredLoan(20);
		ut.chooseSellOnline(2);
		ut.terms.click();
		ut.signup.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		Assert.assertEquals(ut.helpSignup(),"Please complete all the fields before submitting");	
	}

	// Test Case for null Company TurnOver
	@Test
	public void nullCompanyTurnOver(){

		ut.fillFullName("test");
		ut.fillEmail("t.est.c.ap.ita.lfloa.t@gmail.com");
		ut.fillMobNumber("9845220265");
		//new Select(driver.findElement(By.id("city"))).selectByIndex(2);
		ut.chooseCity(2);
		ut.chooseOperation(2);
		ut.fillBusinessName("test");
		//ut.chooseRequiredLoan(20);
		ut.chooseSellOnline(2);
		ut.terms.click();
		ut.signup.click();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		Assert.assertEquals(ut.helpSignup(),"Please complete all the fields before submitting");	
	}

	// Test Case for null Sell Online
	@Test
	public void nullSellOnline(){

		ut.fillFullName("test");
		ut.fillEmail("t.est.c.ap.ita.lfloa.t@gmail.com");
		ut.fillMobNumber("9845220265");
		ut.chooseCity(2);
		ut.chooseOperation(2);
		ut.chooseCompanyTurnover(6);
		ut.fillBusinessName("test");
		//ut.chooseRequiredLoan(20);					
		ut.terms.click();
		ut.signup.click();
		Assert.assertEquals(ut.helpSignup(),"Please complete all the fields before submitting");	
	}
}
